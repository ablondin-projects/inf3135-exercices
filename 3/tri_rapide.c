/**
 * La solution présentée dans ce fichier est inspirée
 * de la page https://en.wikipedia.org/wiki/Quicksort,
 * en utilisant la stratégie proposée par Hoare.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#define LONG_TAB 100

/**
 * Échange les valeurs de s et t.
 */
void echanger(char **s, char **t) {
    char *temp;
    temp = *s;
    *s = *t;
    *t = temp;
}

/**
 * Partitionne le tableau autour de tab[i] et
 * retourne la position finale de tab[i].
 *
 * Le tableau doit contenir au moins un élément.
 */
int partitionner(char *tab[], int i, int j) {
    char *pivot = tab[i];
    int g = i - 1, d = j + 1;
    while (true) {
        do {
            --d;
        } while(strcmp(tab[d], pivot) > 0);
        do {
            ++g;
        } while(strcmp(tab[g], pivot) < 0);
        if (g < d)
            echanger(&tab[g], &tab[d]);
        else
            return d;
    }
}

/**
 * Trie récursivement le tableau donné entre les
 * indices i et j.
 *
 * Il suffit d'abord de partitionner le tableau
 * autour de tab[i]. Comme l'élément est alors
 * placé correctement, on trie récursivement les
 * deux autres portions.
 */
void trierRapidementRec(char *tab[], int i, int j) {
    if (i < j) {
        int m = partitionner(tab, i, j);
        trierRapidementRec(tab, i, m);
        trierRapidementRec(tab, m + 1, j);
    }
}

/**
 * Trie rapidement le tableau donné, en appelant
 * une fonction récursive.
 */
void trierRapidement(char *tab[], int longTab) {
    trierRapidementRec(tab, 0, longTab - 1);
}

/**
 * Fonction main.
 */
int main() {
    char *tableau[] = {"pomme", "banane", "fruit", "fraise", "poire",
                       "orange", "bleuet", "melon", "cerise"};
    int lngTableau = 9;
    trierRapidement(tableau, lngTableau);
    int i;
    for (i = 0; i < lngTableau; ++i) printf("%s\n", tableau[i]);
}
