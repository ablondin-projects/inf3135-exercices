#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define LINE_SIZE 200

float somme(char *line) {
    float s = 0.0;
    char lineCopy[LINE_SIZE];
    strcpy(lineCopy, line);
    char *ps = line, *token;
    while ((token = strsep(&ps, ":")) != NULL) {
        // Je ne fais pas la validation des valeurs
        if (strlen(token) == 0) {
            printf("  Attention! ligne avec champ vide\n");
        }
        s += strtof(token, NULL);
    }
    return s;
}

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Nombre d'arguments invalide\n");
        printf("  USAGE: %s <fichier csv>\n", argv[0]);
        return 1;
    } else {
        FILE *file = fopen(argv[1], "r");
        if (file == NULL) {
            printf("Le fichier %s n'existe pas\n", argv[1]);
            return 2;
        } else {
            char line[LINE_SIZE];
            while (fgets(line, LINE_SIZE - 1, file) != NULL) {
                printf("%lf\n", somme(line));
            }
            return fclose(file);
        }
    }
}
