#include <stdio.h>
#include <math.h>

/**
 * Vecteur en dimension 3.
 */
struct Vec3 {
    float x;
    float y;
    float z;
};

/**
 * Retourne la norme du vecteur v.
 */
float norme(const struct Vec3 *v) {
    return sqrt(v->x * v->x + v->y * v->y + v->z * v->z);
}

/**
 * Retourne le produit scalaire de v1 et de v2.
 */
float produitScalaire(const struct Vec3 *v1, const struct Vec3 *v2) {
    return v1->x * v2->x + v1->y * v2->y + v1->z * v2->z;
}

/**
 * Calcul le produit vectoriel de v1 et v2.
 *
 * Rappelons que si (x1, y1, z1) et (x2, y2, z2) sont deux
 * vecteurs, alors le produit vectoriel de ces deux vecteurs
 * est (y1z2 - y2z1, z1x2 - z2x1, x1y2 - x2y1).
 */
void produitVectoriel(const struct Vec3 *v1, const struct Vec3 *v2, struct Vec3 *res) {
    res->x = v1->y * v2->z - v1->z * v2->y;
    res->y = v1->z * v2->x - v1->x * v2->z;
    res->z = v1->x * v2->y - v1->y * v2->x;
}

/**
 * Calcul la somme de deux vecteurs.
 */
void somme(const struct Vec3 vecteurs[], int nbVecteurs, struct Vec3 *res) {
    int i;
    res->x = res->y = res->z = 0.0;
    for (i = 0; i < nbVecteurs; ++i) {
        res->x += vecteurs[i].x;
        res->y += vecteurs[i].y;
        res->z += vecteurs[i].z;
    }
}

int main() {
    struct Vec3 v1 = {-1.0, 2.0, 3.0};
    struct Vec3 v2 = {3.0, 1.5, -2.5};
    struct Vec3 v3;
    struct Vec3 vecteurs[] = {v1, v2, v1, v1, v2};
    printf("Vecteur v1: (%f, %f, %f)\n", v1.x, v1.y, v1.z);
    printf("Vecteur v2: (%f, %f, %f)\n", v2.x, v2.y, v2.z);
    printf("Norme du vecteur v1: %f\n", norme(&v1));
    printf("Produit scalaire de v1 et v2: %f\n", produitScalaire(&v1, &v2));
    printf("Produit scalaire de v1 et v2: %f\n", produitScalaire(&v1, &v2));
    produitVectoriel(&v1, &v2, &v3);
    printf("Produit vectoriel de v1 et v2: (%f, %f, %f)\n", v3.x, v3.y, v3.z);
    somme(vecteurs, 5, &v3);
    printf("v1 + v2 + v1 + v1 + v2 = (%f, %f, %f)\n", v3.x, v3.y, v3.z);
}
