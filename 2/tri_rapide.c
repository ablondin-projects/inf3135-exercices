/**
 * La solution présentée dans ce fichier est inspirée
 * de la page https://en.wikipedia.org/wiki/Quicksort,
 * en utilisant la stratégie proposée par Hoare.
 *
 * Pour tester si elle est fonctionnelle, j'utilise la
 * fonction ``rand()`` qui retourne un nombre entier
 * au hasard. Ainsi, en écrivant
 *
 *     rand() % LONG_TAB;
 *
 * on génère un entier entre 0 et LONG_TAB - 1.
 */
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#define LONG_TAB 100

/**
 * Échange les valeurs de i et j.
 */
void echanger(int *i, int *j) {
    int temp;
    temp = *i;
    *i = *j;
    *j = temp;
}

/**
 * Partitionne le tableau autour de tab[i] et
 * retourne la position finale de tab[i].
 *
 * Le tableau doit contenir au moins un élément.
 */
int partitionner(int tab[], int i, int j) {
    int pivot = tab[i];
    int g = i - 1, d = j + 1;
    while (true) {
        do {
            --d;
        } while(tab[d] > pivot);
        do {
            ++g;
        } while(tab[g] < pivot);
        if (g < d)
            echanger(&tab[g], &tab[d]);
        else
            return d;
    }
}

/**
 * Trie récursivement le tableau donné entre les
 * indices i et j.
 *
 * Il suffit d'abord de partitionner le tableau
 * autour de tab[i]. Comme l'élément est alors
 * placé correctement, on trie récursivement les
 * deux autres portions.
 */
void trierRapidementRec(int tab[], int i, int j) {
    if (i < j) {
        int m = partitionner(tab, i, j);
        trierRapidementRec(tab, i, m);
        trierRapidementRec(tab, m + 1, j);
    }
}

/**
 * Trie rapidement le tableau donné, en appelant
 * une fonction récursive.
 */
void trierRapidement(int tab[], int longTab) {
    trierRapidementRec(tab, 0, longTab - 1);
}

/**
 * Fonction main.
 */
int main() {
    int tableau[LONG_TAB];
    int i;
    for (i = 0; i < LONG_TAB; ++i) {
        tableau[i] = rand() % LONG_TAB;
    }
    trierRapidement(tableau, LONG_TAB);
    for (i = 0; i < LONG_TAB; ++i) printf("%d ", tableau[i]);
}
