#include <stdio.h>
#include <stdbool.h>

bool estBissextile(int annee) {
    return annee % 4 == 0 && (annee % 100 != 0 || annee % 400 == 0);
}

int main() {
    int annee;
    printf("%s\n", "Année bissextiles :");
    for (annee = 0; annee < 2020; ++annee) {
        if (estBissextile(annee)) printf("%d ", annee);
    }
}
