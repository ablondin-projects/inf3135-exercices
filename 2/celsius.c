#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[]) {
    if (argc != 2) {
        printf("Nombre d'arguments invalides\n");
        printf("  USAGE: %s <température en farenheit>", argv[0]);
        return 1;
    } else {
        char *reste;
        float farenheit = strtof(argv[1], &reste);
        if (reste == argv[1] || *reste != '\0') {
            printf("Le deuxième argument doit être un nombre");
            printf("  USAGE: %s <température en farenheit>", argv[0]);
            return 2;
        } else {
            printf("%f", 5 * (farenheit - 32.0) / 9.0);
            return 0;
        }
    }
}
